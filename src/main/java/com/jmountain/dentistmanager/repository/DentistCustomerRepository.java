package com.jmountain.dentistmanager.repository;

import com.jmountain.dentistmanager.entity.DentistCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DentistCustomerRepository extends JpaRepository<DentistCustomer, Long> {
}
