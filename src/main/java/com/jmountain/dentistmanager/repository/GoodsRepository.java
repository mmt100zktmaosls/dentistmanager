package com.jmountain.dentistmanager.repository;

import com.jmountain.dentistmanager.entity.Goods;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoodsRepository extends JpaRepository<Goods, Long>{

}