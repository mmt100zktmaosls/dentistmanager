package com.jmountain.dentistmanager.controller;

import com.jmountain.dentistmanager.model.DentistCustomerItem;
import com.jmountain.dentistmanager.model.DentistCustomerRequest;
import com.jmountain.dentistmanager.model.DentistCustomerTotalCureTimeRequest;
import com.jmountain.dentistmanager.service.DentistCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/DentistCustomer")
public class DentistCustomerController {
    private final DentistCustomerService dentistCustomerService;

    @PostMapping("/data")
    public String setDentistCustomer(@RequestBody @Valid DentistCustomerRequest request) {
        dentistCustomerService.setDentistCustomer(request);

        return "OK";
    }
    @GetMapping("/all")
    public List<DentistCustomerItem> getDentistCustomers() {
        List<DentistCustomerItem> result = dentistCustomerService.getCustomers();

        return result;
    }
    @PutMapping("/total-cure-time/id/{id}")
    public String putDentistCustomerTotalCureTime(@PathVariable long id, @RequestBody @Valid DentistCustomerTotalCureTimeRequest request) {

        dentistCustomerService.putDentistCustomerTotalCureTime(id, request);

        return "OK";
    }
    @PutMapping("/time-out/id/{id}")
    public String putDentistCustomerTimeOut(@PathVariable long id) {
        dentistCustomerService.putDentistCustomerTimeOut(id);

        return "OK";
    }
    @DeleteMapping("/sign-out/id/{id}")
    public String delDentistCustomer(@PathVariable long id) {
        dentistCustomerService.delDentistCustomer(id);

        return "OK";
    }
}
