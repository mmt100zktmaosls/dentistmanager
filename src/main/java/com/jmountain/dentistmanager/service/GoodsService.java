package com.jmountain.dentistmanager.service;

import com.jmountain.dentistmanager.entity.Goods;
import com.jmountain.dentistmanager.model.GoodsItem;
import com.jmountain.dentistmanager.repository.GoodsRepository;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GoodsService {

    private final GoodsRepository goodsRepository;

    public List<GoodsItem> getGoods() {
        List<Goods> originList = goodsRepository.findAll();

        List<GoodsItem> result = new LinkedList<>();

        for (Goods item : originList) {
            GoodsItem addItem = new GoodsItem();
            addItem.setId(item.getId());
            addItem.setGoodsName(item.getGoodsName());
            addItem.setExpirationDate(item.getExpirationDate());

            String statusName = "정상";
            if (item.getExpirationDate().isBefore(LocalDate.now())) {
                statusName = "폐기";
            } else if (item.getExpirationDate().isBefore(LocalDate.now())) {
                statusName = "임박";
            }
            addItem.setGoodsStatus(statusName);
            result.add(addItem);
        }
        return result;
    }
}
