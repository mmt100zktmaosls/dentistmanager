package com.jmountain.dentistmanager.service;

import com.jmountain.dentistmanager.entity.DentistCustomer;
import com.jmountain.dentistmanager.model.DentistCustomerItem;
import com.jmountain.dentistmanager.model.DentistCustomerRequest;
import com.jmountain.dentistmanager.model.DentistCustomerTotalCureTimeRequest;
import com.jmountain.dentistmanager.repository.DentistCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DentistCustomerService {

    private final DentistCustomerRepository dentistCustomerRepository;

    public void setDentistCustomer(DentistCustomerRequest request) {
        DentistCustomer addData = new DentistCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setDiseaseName(request.getDiseaseName());
        addData.setReservationTime(request.getReservationTime());
        addData.setMedicalExpenses(request.getMedicalExpenses());

        dentistCustomerRepository.save(addData);
    }

    public List<DentistCustomerItem> getCustomers() {
        List<DentistCustomer> originList = dentistCustomerRepository.findAll();

        List<DentistCustomerItem> result = new LinkedList<>();

        for (DentistCustomer item : originList) {
            DentistCustomerItem addItem = new DentistCustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerInfo(addItem.getCustomerInfo());

            result.add(addItem);
        }

        return result;
    }

    public void putDentistCustomerTotalCureTime(long id, DentistCustomerTotalCureTimeRequest request) {
        DentistCustomer originData = dentistCustomerRepository.findById(id).orElseThrow();
        originData.setTotalCureTime(request.getTotalCureTime());
        originData.setTimeEnd(originData.getTimeStart().plusHours(request.getTotalCureTime()));

        dentistCustomerRepository.save(originData);

    }

    public void putDentistCustomerTimeOut(long id) {
        DentistCustomer originData = dentistCustomerRepository.findById(id).orElseThrow();
        originData.setTimeOut(LocalDateTime.now());

        dentistCustomerRepository.save(originData);
    }

    public void delDentistCustomer(long id) {
        dentistCustomerRepository.deleteById(id);
    }
}
