package com.jmountain.dentistmanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DentistCustomerItem {

    private Long id;

    private String customerInfo;

}
