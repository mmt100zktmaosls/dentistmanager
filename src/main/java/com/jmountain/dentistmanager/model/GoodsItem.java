package com.jmountain.dentistmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class GoodsItem {
    private Long id;
    private String goodsName;
    private LocalDate expirationDate;
    private String goodsStatus;

}
