package com.jmountain.dentistmanager.model;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DentistCustomerTotalCureTimeRequest {
    @NotNull
    private Integer totalCureTime;
}
