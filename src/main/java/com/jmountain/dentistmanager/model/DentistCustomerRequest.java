package com.jmountain.dentistmanager.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class DentistCustomerRequest {

    @NotNull
    @Length(min = 1, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 5, max = 25)
    private String customerPhone;

    @NotNull
    private String diseaseName;

    @NotNull
    private String reservationTime;

    @NotNull
    private String medicalExpenses;

    @NotNull
    private Integer totalCureTime;


}
