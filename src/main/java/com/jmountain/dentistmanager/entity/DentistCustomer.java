package com.jmountain.dentistmanager.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class DentistCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false,length = 20)
    private String customerPhone;

    @Column(nullable = false)
    private String diseaseName;

    @Column(nullable = false)
    private String reservationTime;

    @Column(nullable = false)
    private String medicalExpenses;
    @Column(nullable = false)
    private Integer totalCureTime;

    @Column(nullable = false)
    private LocalDateTime timeStart;

    @Column(nullable = false)
    private LocalDateTime timeEnd;

    private LocalDateTime timeOut;


}
